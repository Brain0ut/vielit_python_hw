import os
lev= os.getcwd().count(os.sep)
for root, dirs, files in os.walk(os.getcwd()):
    level = root.count(os.sep)
    indent = '|--' * (level-lev)
    print(f'{indent}[{root}]')
    sub_indent = '|--' * (level - lev + 1)
    # print(root, files, level, indent, sub_indent)
    for file in files:
        print(f'{sub_indent}{file}')
