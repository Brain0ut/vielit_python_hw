

def mult_tbl(x):
    tbl = []
    for i in x:
        try:
            i = int(i)
            for y in range(1, 10):
                tbl.append(f'{i} * {y} = {i*y}')
                tbl.append('\t')
            tbl.append('\n')
        except:
            pass
    return tbl

y = list(input('Введите число (или числа через запятую), для которого хотите получить таблицу умножения: '))

for i in mult_tbl(y):
    print(f'{i}', end='')
